package com.example.customviewtest;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

public class CustomView extends ConstraintLayout {

    private ConstraintLayout layoutHint, layoutMainText;
    private TextView tvMain, tvHint;

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    public CustomView(Context context) {
        super(context, null);
        initialize(context, null);
    }

    private void initialize(Context context, @Nullable AttributeSet attrs) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_view, this, true);

        layoutHint = view.findViewById(R.id.layout_hint);
        layoutMainText = view.findViewById(R.id.layout_main_text);

        tvMain = view.findViewById(R.id.tv_main);
        tvHint = view.findViewById(R.id.tv_hint);

        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(
                    attrs,
                    R.styleable.CustomView,
                    0,
                    0
            );

            setText(typedArray.getString(R.styleable.CustomView_text));
            setHint(typedArray.getString(R.styleable.CustomView_textHint));
            setEnabled(typedArray.getBoolean(R.styleable.CustomView_enable, true));

            typedArray.recycle();
        }
    }

    public void setText(String text) {
        if (tvMain != null) tvMain.setText(text);
    }

    public void setHint(String hint) {
        if (tvHint != null) {
            if (hint == null) {
                layoutHint.setVisibility(GONE);
                tvHint.setText(null);
            } else {
                layoutHint.setVisibility(VISIBLE);
                tvHint.setText(hint);
            }
        }
    }

    public void setEnabled(boolean enabled) {
        int textColor;
        int layoutColor;

        GradientDrawable drawable = (GradientDrawable) layoutMainText.getBackground();

        if (enabled) {
            textColor = getResources().getColor(R.color.colorAccent);
            layoutColor = getResources().getColor(R.color.colorAccent);
        } else {
            textColor = getResources().getColor(R.color.color_grey);
            layoutColor = getResources().getColor(R.color.color_dark_grey);
        }

        drawable.setStroke(2, layoutColor);
        tvHint.setTextColor(textColor);
        tvMain.setTextColor(textColor);
    }
}
